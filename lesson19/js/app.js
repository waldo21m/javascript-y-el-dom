const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

const input = document.getElementById('input');

input.addEventListener('keydown', function (e) {
    console.log('Tecla presionada' + e.key + ' con un código ' + e.keyCode);
});