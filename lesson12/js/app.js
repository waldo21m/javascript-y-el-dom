const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
// console.log(description.textContent);
description.textContent = 'Listado de cursos';
description.innerHTML = '<strong>' + description.textContent + '</strong>';

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}