const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

const row = document.querySelector('.row');

const form = document.getElementById('course-form');

form.addEventListener('submit', function(e) {
    e.preventDefault();

    let title = document.getElementById('title-form').value;
    let description = document.getElementById('description-form').value;

    createCard(title, description);
});

// Hacerlo de esta forma eliminará solo el último div. Esto no es práctico
let div = null;

function createCard(title, description) {
    div = document.createElement('div');
    div.className = 'col-sm-6 col-md-4';

    let thumbnail = document.createElement('div');
    thumbnail.className = 'thumbnail';

    let caption = document.createElement('div');
    caption.className = 'caption';

    let h3 = document.createElement('h3');
    h3.textContent = title;

    let p1 = document.createElement('p');
    p1.textContent = description;

    let p2 = document.createElement('p');
    let a = document.createElement('a');
    a.className = 'btn btn-danger';
    a.textContent = 'Eliminar';

    p2.addEventListener('click', deleteCard);
    
    p2.appendChild(a);
    caption.appendChild(h3);
    caption.appendChild(p1);
    caption.appendChild(p2);

    thumbnail.appendChild(caption);
    div.appendChild(thumbnail);

    row.appendChild(div);
}

function deleteCard(e) {
    // El padre y el elemento a eliminar (hijo)
    let ancestor = getAncestors(e.target, 4);
    row.removeChild(ancestor);

    // Forma no práctica
    // row.removeChild(div);
}

function getAncestors(ancestor, level) {
    if (!level) {
        return ancestor;
    }
    level--;
    return getAncestors(ancestor.parentElement, level);
}

function createCardByInnerHTML(title, description) {
    let html = `<div class="col-sm-6 col-md-4">\
                    <div class="thumbnail">\
                        <div class="caption">\
                            <h3 id="title-card"> ${title} </h3>\
                            <p id="description-card"> ${description} </p>\
                            <p><a href="#" class="btn btn-danger">Acción</a></p>\
                        </div>\
                    </div>\
                </div>`;
    row.innerHTML += html;            
}