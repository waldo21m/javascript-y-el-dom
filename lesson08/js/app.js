const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// Obtener elementos por CSS

// Por id
// const element = document.querySelector('#first-course');

// Por clase
// const element = document.querySelector('.list-group-item');

// Por etiqueta
// const element = document.querySelector('li');

// Buscar elementos mediantes reglas de CSS más complejas
const element = document.querySelector('div.row > ul.list-group > li')
console.log(element);

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');

// Obtener elementos por CSS con querySelectorAll
// const items = document.querySelectorAll('li');

// Obtener elementos li pares por CSS
// const items = document.querySelectorAll('li:nth-child(odd)');
// Para que sean impares, usamos even
const items = document.querySelectorAll('li:nth-child(even)');

// console.log(items);
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}