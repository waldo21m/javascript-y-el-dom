const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

const list = document.querySelector('ul');

// Cantidad de elementos hijos y es de solo lectura
// console.log(list.childElementCount); 

// Ver la lista de hijos
// console.log(list.children);

// Iterar la lista de los elementos hijos
// j = list.childElementCount;
// for (var i = 0; i < j; i++) {
//     console.log(list.children[i]);
// }

// De igual forma, podemos obtener un elemento hijo mediante un indice
console.log(list.children[1]);

// Obtener el primer y último elemento hijo
console.log(list.firstElementChild.innerHTML); // Accediendo al atributo innerHTML
console.log(list.lastElementChild.innerHTML);