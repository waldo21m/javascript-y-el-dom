const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

// Buscar elementos mediantes reglas de CSS más complejas
const element = document.querySelector('div.row > ul.list-group > li')
// Visualizar el ul por ser el elemento padre
console.log(element.parentElement);
// Visualizar el div por el abuelo
// console.log(element.parentElement.parentElement);

// Visualizar el hermano siguiente(x2)
console.log(element.nextElementSibling.nextElementSibling.innerHTML);
// En el caso que el elemento no tenga hermano, el atributo será null

// Visualizar el hermano anterior
const lastElement = document.getElementById('last-course');
console.log(lastElement.previousElementSibling.innerHTML);