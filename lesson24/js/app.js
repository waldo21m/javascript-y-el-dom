const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

const form = document.getElementById('course-form');

form.addEventListener('submit', function(e) {
    e.preventDefault();

    let title = document.getElementById('title-form').value;
    let description = document.getElementById('description-form').value;

    console.log(title);
    console.log(description);
});

// Pequeño refactor
for(let element of document.querySelectorAll('*')) {
    element.addEventListener('click', showMessages);
}

// const element = document.querySelector('li');
// const list = document.querySelector('ul');
// const divRow = document.querySelector('.row');
// const divContainer = document.querySelector('.container');
// const body = document.querySelector('body');

// element.addEventListener('click', showMessages);
// list.addEventListener('click', showMessages);
// divRow.addEventListener('click', showMessages);
// divContainer.addEventListener('click', showMessages);
// body.addEventListener('click', showMessages);

// element.addEventListener('click', function() {
//     console.log('Elemento!');
// });

// list.addEventListener('click', function() {
//     console.log('Lista!');
// });

// divRow.addEventListener('click', function() {
//     console.log('Div Row!');
// });

// divContainer.addEventListener('click', function() {
//     console.log('Div Container!');
// });

// body.addEventListener('click', function() {
//     console.log('Body!');
// });

function showMessages(e) {
    console.log("Elemento actual: " + this.tagName);
    console.log("Elemento que disparo el evento: " + e.target.tagName);
    console.log("\n");
}