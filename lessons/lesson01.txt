Introducción al curso del DOM

Actualmente tenemos a JS por todos lados: en servidores, en robots e inclusive
podemos utilizarlo para programar nuestros Drones. Sin embargo, el inicio de
estos lenguajes fue en nuestros navegadores junto a tecnologías como HTML y CSS.

En este curso nos enfocaremos a trabajar JS en el lado del cliente, es decir,
con los navegadores mediante videos conceptuales y prácticos, te enseñaremos las
diferentes formas en las cuales puedes crear sitios webs dinámicos. Cubriremos
temas muy interesantes comenzando desde lo básico, como se conforma nuestro
maquetado de nuestras páginas web, cuales son los elementos principales dentro
de los navegadores, aprender a manipular elementos de nuestros sitios web, etc.
De igual forma, aprenderemos a crear, editar y eliminar elementos, ya sea con
eventos del teclado o con eventos del mouse.

También tocaremos temas más avanzados y te explicaremos como se comportan los
eventos en nuestras páginas web y como estos cargan en el navegador.

Los requerimientos de este curso es conocer HTML y CSS de forma básica y JS de
forma intermedia. Al termino de este curso podremos crear controles complejos,
optimizar y mejorar el rendimiento de un sitio web y en general para que tomes
el control del árbol que representa todos tus elementos HTML en tu página web.