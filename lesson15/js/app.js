const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

const button = document.querySelector('.btn.btn-primary');

// button.addEventListener('click', function() {
//     console.log('Hola Mundo!');
// });

button.addEventListener('dblclick', function() {
    console.log('Hola Mundo!');
});