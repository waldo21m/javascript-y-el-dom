const title = document.getElementById('title');
title.innerHTML = "Cursos";

const description = document.getElementById('description');
description.innerHTML = "Listado de cursos";

// const items = document.getElementsByClassName('list-group-item');
// const items = document.getElementsByTagName('li');
const items = document.querySelectorAll('li:nth-child(even)');
let j = items.length;
for (var i = 0; i < j; i++) {
    let element = items[i];
    element.style.background = '#f2f2f2';        
}

const form = document.getElementById('course-form');

form.addEventListener('submit', function(e) {
    e.preventDefault();

    let title = document.getElementById('title-form').value;
    let description = document.getElementById('description-form').value;

    console.log(title);
    console.log(description);
});

const element = document.querySelector('li');
const list = document.querySelector('ul');

element.addEventListener('click', function(e) {
    console.log('Click sobre el elemento!');
    e.stopPropagation();
});

list.addEventListener('click', function() {
    console.log('Click sobre la lista!');
});